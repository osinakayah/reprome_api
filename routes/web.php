<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $states = \App\State::get();

    return view('welcome', ['states' => $states]);
});

Route::post('save_state', function (\Illuminate\Http\Request $request){
    $state = new \App\State();
    $state->state = $request->get('statename');
    $state->save();
    redirect('/');
});

Route::post('save_lga', function (\Illuminate\Http\Request $request){
    $state = $request->get('state');
    $lga   = $request->get('lganame');


    \App\LGA::create(['state_id'    =>  $state, 'name'   => $lga]);
});

Route::post('save_clinic', function (\Illuminate\Http\Request $request){

    $lga   = $request->get('lga_id');

    $name  = $request->get('name');
    $address = $request->get('address');
    $contact = $request->get('contact');
    $phoneNumber = $request->get('phone_number');

    $clinic = new \App\Clinic();
    $clinic->name = $name;
    $clinic->lga_id = $lga;
    $clinic->location = $address;
    $clinic->doctor = $contact;
    $clinic->number = $phoneNumber;
    $clinic->save();
});

Route::get('get_lgas_under_state/{stateId}', function ($staeId){
    $lgas = \App\LGA::where('state_id', $staeId)->get(['state']);
    return response()->json($lgas);
});

Route::get('my_json', function (){
    $states = \App\State::get();
    $data = [];
    foreach ($states as $state){

        $lgas = $state->getLGA;

        foreach ($lgas as $lga){

            $clinics = $lga->getClinics;

            array_push($data, $state);
        }
    }

    return response()->json($data);
});

//Auth::routes();

Route::get('/home', 'HomeController@index');
