<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', function (Request $request){
    $user = new \App\User();
    $user->name = $request->get('name', '');
    $user->email = $request->get('email', '');
    $user->password = bcrypt($request->get('password'));

    if($user->save()){
        return response()->json(['status'=>'ok', 'email'=> $user->email]);
    }else{
        return response()->json(['status'=>'error', 'email'=> '']);
    }

});

Route::post('login', function (Request $request){
    if(Auth::attempt(['email'=> $request->get('email'), 'password'=> $request->get('password')])){
        return response()->json(['status'=>'ok', 'email'=> $request->get('email')]);
    }else{
        return response()->json(['status'=>'error', 'email'=> '']);
    }
});

Route::post('recommendation', function (Request $request){
    \DB::table('recommendations')->insert([
        'name'      => $request->get('name'),
        'message'   => $request->get('message'),
    ]);
    return response()->json(['status'=>'ok', 'email'=> $request->get('email')]);
});