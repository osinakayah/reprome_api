<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LGA extends Model
{
    protected $table = 'lgas';
    protected $fillable = ['state_id', 'name'];

    public function getClinics(){
        return $this->hasMany('App\Clinic', 'lga_id');
    }
}
