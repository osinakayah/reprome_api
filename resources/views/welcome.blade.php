<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reprome</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
    </head>
    <body>
        <div class="container">
            {{--Save state form--}}
            <div class="row col-lg-offset-2 col-lg-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add new state</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="save_state">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="statename" class="col-sm-2 control-label">State Name</label>
                                <div class="col-sm-10">
                                    <input name="statename" type="text" class="form-control" id="statename" placeholder="Enter State Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--Save LGA form--}}
            <div class="row col-lg-offset-2 col-lg-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add New LGA</div>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" method="post" action="save_lga">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Select State</label>
                                <select name="state" class="form-control">
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}">{{$state->state}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="lganame" class="col-sm-2 control-label">LGA Name</label>
                                <div class="col-sm-10">
                                    <input name="lganame" type="text" class="form-control" id="lganame" placeholder="Enter LGA Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--Save clinic form--}}
            <div class="row col-lg-offset-2 col-lg-8">
                <div class="panel-primary panel">
                    <div class="panel-heading">Add clinic</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" action="save_clinic">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Select State</label>
                                <select id="select_state_enter_clinic_input" name="state" class="form-control">
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}">{{$state->state}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Select LGA</label>
                                <select id="select_lga_enter_clinic_input" name="lga_id" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label for="statename" class="col-sm-2 control-label">Clinic Name</label>
                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" id="statename" placeholder="Enter Clinic Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="statename" class="col-sm-2 control-label">Clinic address</label>
                                <div class="col-sm-10">
                                    <input name="address" type="text" class="form-control" id="statename" placeholder="Enter Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="statename" class="col-sm-2 control-label">Name of contact</label>
                                <div class="col-sm-10">
                                    <input name="contact" type="text" class="form-control" id="statename" placeholder="Enter Contact Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="statename" class="col-sm-2 control-label">Phone number</label>
                                <div class="col-sm-10">
                                    <input name="phone_number" type="text" class="form-control" id="statename" placeholder="Enter phone number">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <link rel="stylesheet" href="">
    <script src="{{URL::asset('js/jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: 'get_lgas_under_state/15',
                success: (response)=>{
                    $lgaSelect = $('#select_lga_enter_clinic_input');
                    $lgaSelect.empty();
                    for(let q = response.length - 1; q >= 0; q--){
                        $lgaSelect.append('<option value="'+response[q].id+'">'+response[q].name+'</option>')
                    }
                }
            });
            $('#select_state_enter_clinic_input').change(function (e) {
                let stateId = $(this).val();
                $.ajax({
                    url: 'get_lgas_under_state/'+stateId,
                    success: (response)=>{
                        $lgaSelect = $('#select_lga_enter_clinic_input');
                        $lgaSelect.empty();
                        for(let q = response.length - 1; q >= 0; q--){
                            $lgaSelect.append('<option value="'+response[q].id+'">'+response[q].name+'</option>')
                        }
                    }
                });
            });
        });
    </script>
    </body>
</html>
